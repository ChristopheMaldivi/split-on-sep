package org.tof

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files
import java.util.*

class SplitOnSepTest {

    private val outputdirFile = File("/tmp/${UUID.randomUUID()}")
    private val separatorString = "0123456789"

    @BeforeEach
    fun setup() {
        outputdirFile.mkdir()
    }

    @AfterEach
    fun cleanup() {
        Files.walk(outputdirFile.toPath()).map {
            it.toFile().delete()
        }
        outputdirFile.delete()
    }

    @Test
    fun `split in two file with one separator`() {
        // given
        val inputString = "a${separatorString}b"

        // when
        SplitOnSep.split(inputString.toByteArray(Charsets.UTF_8), separatorString.toByteArray(Charsets.UTF_8), outputdirFile)

        // then
        assertEquals(outputdirFile.listFiles()?.size, 2)
        assertEquals(File("${outputdirFile.absolutePath}/1").readText(Charsets.UTF_8), "a")
        assertEquals(File("${outputdirFile.absolutePath}/2").readText(Charsets.UTF_8), "b")
    }

    @Test
    fun `split in files with one separator`() {
        // given
        val seed = "abcdefghijklmnopqrstuvwxyz"
        val inputString = generateInput(seed)

        // when
        SplitOnSep.split(inputString.toByteArray(Charsets.UTF_8), separatorString.toByteArray(Charsets.UTF_8), outputdirFile)

        // then
        val files = outputdirFile.listFiles()
        assertEquals(files?.size, seed.length)
        files?.indices?.forEach {
            assertEquals(File("${outputdirFile.absolutePath}/${it + 1}").readText(Charsets.UTF_8), encodePattern(seed[it]))
        }
    }

    private fun generateInput(seed: String): String {
        return seed
                .map { "${encodePattern(it)}$separatorString" }
                .reduce { merge:String, s: String -> merge + s }
    }

    private fun encodePattern(c: Char) = " # ${c}${c}*"
}