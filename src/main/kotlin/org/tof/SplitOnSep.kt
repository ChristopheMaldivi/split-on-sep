package org.tof

import java.io.File

object SplitOnSep {

    fun split(input: ByteArray, separator: ByteArray, outputDir: File) {
        var hitCount = 0
        var previousIndex = 0
        var currentIndex: Int
        do {
            currentIndex = indexOf(input, previousIndex, separator)
            if (currentIndex < 0) {
                hitCount++
                write(input, previousIndex, input.size, outputDir, hitCount)
                return
            }
            hitCount++
            write(input, previousIndex, currentIndex, outputDir, hitCount)
            previousIndex = currentIndex + separator.size
        } while (currentIndex < input.size - separator.size)
    }

    private fun write(input: ByteArray, startIndexInclude: Int, stopIndexExclude: Int, outputDir: File, hitCount: Int) {
        val outputBytes = input.sliceArray(IntRange(startIndexInclude, stopIndexExclude - 1))
        val file = File("${outputDir.absolutePath}/$hitCount")
        file.createNewFile()
        file.writeBytes(outputBytes)
    }

    private fun indexOf(input: ByteArray, startIndexInclude: Int, separator: ByteArray): Int {
        for (i in startIndexInclude until input.size - separator.size + 1) {
            var found = true
            for (j in separator.indices) {
                if (input[i + j] != separator[j]) {
                    found = false
                    break
                }
            }
            if (found) return i
        }
        return -1
    }
}
