package org.tof

import io.quarkus.runtime.StartupEvent
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.slf4j.LoggerFactory
import java.io.File
import javax.enterprise.event.Observes
import javax.inject.Singleton
import kotlin.concurrent.thread
import kotlin.system.exitProcess

@Singleton
class AppLaunch {

    private val log = LoggerFactory.getLogger(AppLaunch::class.java)

    @ConfigProperty(name = "fileToParse")
    lateinit var fileToParse: String

    @ConfigProperty(name = "outputdirAbsPath")
    lateinit var outputDir: String

    @ConfigProperty(name = "separatorString")
    lateinit var separator: String

    fun atAppLaunch(@Observes event: StartupEvent) {
        if (empty(fileToParse) || empty(outputDir) || empty(separator)) {
            showHelpAndExit()
        }
        val fileToParseFile = File(fileToParse)
        if (!fileToParseFile.isFile) {
            log.error("file '$fileToParse' is not a file")
            showHelpAndExit()
            return
        }
        val outputDirFile = File(outputDir)
        if (outputDirFile.exists() && !outputDirFile.isDirectory) {
            log.error("file '$outputDirFile' is not a directory")
            showHelpAndExit()
            return
        } else {
            outputDirFile.mkdir()
        }
        SplitOnSep.split(
                fileToParseFile.readBytes(),
                separator.toByteArray(Charsets.UTF_8),
                outputDirFile)
        exit()
    }

    private fun showHelpAndExit() {
        log.info("Usage: split-on-sep fileToParse separatorString [outputdirAbsPath]")
        exit(true)
    }

    private fun exit(error: Boolean = false) {
        thread { exitProcess(if (error) -1 else 0) }
    }

    private fun empty(str: String?): Boolean {
        val empty = str == null || str.isBlank()
        if (empty) {
            log.error("parameter '$str' is invalid")
        }
        return empty
    }
}